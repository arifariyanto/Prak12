<?php
    include_once 'top.php';
    require_once 'classproses.php';
    $obj_peserta = new Peserta();
    $_idedit = $_GET['id'];

    if(!empty($_idedit)){
        $data = $obj_peserta->findByID($_idedit);
    }else{
        $data = [] ;
    }

?>

<form class="form-horizontal" method="POST" action="classproses.php">
  <fieldset>
    <!-- Form Name -->
    <legend>Form Entry Peserta</legend>
    <!-- Text input-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="nomor">nomor</label>
          <div class="col-md-2">
             <input id="nomor" name="nomor" placeholder="nomor" class="form-control input-md" type="text"  value="<?php echo $data['nomor']?>">
     </div>
   </div>

   <!-- Text input-->
   <div class="form-group">
      <label class="col-md-4 control-label" for="namalengkap">nama lengkap</label>
        <div class="col-md-5">
          <input id="namalengkap" name="namalengkap" placeholder="nama lengkap" class="form-control input-md" type="text"  value="<?php echo $data['namalengkap']?>">
        </div>
     </div>

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label"
      for="email">email</label>
         <div class="col-md-4">
           <input id="email" name="email" placeholder="email" class="form-control input-md" type="text" value="<?php echo $data['email']?>">
     </div>
   </div>

     <?php
      if (empty($_idedit)){
       ?>
       <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
       <?php
      } else {
         ?>
         <input type="hidden" name="idedit" value="<?php echo $_idedit?>" />
         <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
         <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
      <?php
       } ?>
  </form>
<?php
    include_once 'bottom.php';
?>
